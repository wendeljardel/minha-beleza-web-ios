//
//  MainTableViewController.swift
//  MinhaBelezaWeb
//
//  Created by Wendel Vasconcelos on 25/04/17.
//  Copyright © 2017 mbw. All rights reserved.
//

import UIKit

class MainTableViewController: UITableViewController {

    @IBOutlet weak var btnMenu: UIBarButtonItem!
    @IBOutlet weak var btnSearch: UIBarButtonItem!
    var sallons = [String]()
    var promotions = [String]()
    var ratings = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if self.revealViewController() != nil {
            btnMenu.target = revealViewController()
            btnMenu.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        btnMenu.tintColor = UIColor.white
        btnSearch.tintColor = UIColor.white
        navigationController?.navigationBar.barTintColor = UIColor(red: 255.0/255.0, green: 105.0/255.0, blue: 180.0/255.0, alpha: 1.0)
        let attrs = [NSForegroundColorAttributeName: UIColor.white]
        navigationController?.navigationBar.titleTextAttributes = attrs
        
        
        sallons = ["salao_1.jpg","salao_2.jpg","salao_3.jpg","salao_4.jpg","salao_5.jpg","salao_6.jpg","salao_7.jpg"]
        promotions = ["35% OFF","","50% OFF","","15% OFF","",""]
        ratings = ["5.0","4.5","4.5","5.0","4.0","4.5","5.0"]
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return sallons.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MainTableViewCell
        cell.imgSalon.image = UIImage(named: sallons[indexPath.row])
        cell.rating.text = ratings[indexPath.row]
        if promotions[indexPath.row] != ""{
            cell.promotion.text = promotions[indexPath.row]
        }else{
            cell.promotion.isHidden = true
        }
        
        cell.name.text = "Salao00"+String(indexPath.row)
        cell.location.text = "Aldeota-Fortaleza"
        cell.btnFavorite.tag = indexPath.row
        cell.btnFavorite.addTarget(self, action: #selector(setFavorite), for: .touchUpInside)
        

        

        // Configure the cell...

        return cell
    }
    
    @IBAction func setFavorite(sender: UIButton){
        
        sender.setImage(UIImage(named:"like-filled"), for: .normal)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
