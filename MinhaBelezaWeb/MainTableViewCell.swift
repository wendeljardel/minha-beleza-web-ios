//
//  MainTableViewCell.swift
//  MinhaBelezaWeb
//
//  Created by Wendel Vasconcelos on 26/04/17.
//  Copyright © 2017 mbw. All rights reserved.
//

import UIKit

class MainTableViewCell: UITableViewCell {

    
    @IBOutlet weak var promotion: UILabel!
    @IBOutlet weak var btnFavorite: UIButton!
    @IBOutlet weak var imgSalon: UIImageView!
    @IBOutlet weak var location: UILabel!
    @IBOutlet weak var rating: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
